const express = require('express')
const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.json({type: '*/*'}))

// Router Setup
require('./router')(app)

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  let err = new Error('Not Found')
  err.status = 404

  res.status(err.status).json({
    error: {
      message: err.message,
      status: err.status
    }
  })
})

app.listen(3001, () => console.log('App listening on port 3001'))
