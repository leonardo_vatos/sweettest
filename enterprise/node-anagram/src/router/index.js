// Routers
const AnagramRouter = require('./anagram/anagram.router')

// Router
module.exports = function (app) {
  app.use('/api/anagram', AnagramRouter)
}
