// Anagram Router
const anagramRouter = require('express').Router()

// Anagram Controller
const AnagramController = require('../../controllers/anagram/anagram.controller')

// Anagram Routes
anagramRouter
  .get('/ping', AnagramController.ping)
/**
     * @api {get} /find Find Anagrams
     * @apiName FindAnagrams
     * @apiDescription This endpoint will find all anagrams in the english dictionary based on the string sent
     * @apiGroup Anagram
     *
     * @apiParam (query) {String} word
     *
     * @apiExample {curl} Example usage:
     *   curl -X GET -H "Content-Type: application/json" http://localhost:3001/find?word=test
     *
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   [
     *      "word1",
     *      "word2",
     *      "word3"
     *   ]
     */
  .get('/find', AnagramController.find)

/**
     * @api {get} /compare Compare Anagrams
     * @apiName CompareAnagrams
     * @apiDescription This endpoint will receive two words, and compare them to see if they are anagrams
     * @apiGroup Anagram
     *
     * @apiParam (query) {String} word1
     * @apiParam (query) {String} word2
     *
     * @apiExample {curl} Example usage:
     *   curl -X GET -H "Content-Type: application/json" http://localhost:3001/compare?word1=test&word2=tset
     *
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   false
     */
  .get('/compare', AnagramController.compare)

// Export configured Anagram Router
module.exports = anagramRouter
