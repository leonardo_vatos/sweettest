/**
 * AnagramHelper class with static functions to deal with all anagram issues
 */

class AnagramHelper {
  constructor (wordsList) {
    this.wordListByFirstCharacter = this.getWordListByFirstCharacter(wordsList)
    this.wordListByLength = this.getWordListByLength(wordsList)
  }

  /**
     * Get all anagrams of the given word
     * @param {string }wordToCheck
     * @return {Array}
     */
  getAllAnagrams (wordToCheck) {
    let organized = []
    // Check if the word is in the list
    if (!this.isAnagram(wordToCheck)) {
      return organized
    }

    let wordToCheckSorted = this.cleanWord(wordToCheck)
    let filteredList = this.wordListByLength[wordToCheck.length]
    for (let i = 0; i < filteredList.length; i++) {
      let anagramWord = this.cleanWord(filteredList[i])
      if (wordToCheckSorted === anagramWord && filteredList[i] !== wordToCheck) {
        organized.push(filteredList[i])
      }
    }
    return organized
  }

  /**
     * Check if two words are anagrams between them
     * @param {string} word1
     * @param {string} word2
     * @return {boolean}
     */
  checkAnagramWords (word1, word2) {
    if (!this.isAnagram(word1) || !this.isAnagram(word2) || word1.length !== word2.length) {
      return false
    }

    let word1Sorted = this.cleanWord(word1)
    let word2Sorted = this.cleanWord(word2)

    return word1Sorted === word2Sorted
  }

  /**
     * Group all the words in an object using the first character as key
     * @param {Array} completeList - List of all words
     * @return {Object} - indexed by the first character of the words
     */
  getWordListByFirstCharacter (completeList) {
    let wordListByFirstCharacter = {}
    completeList.map(item => {
      if (!wordListByFirstCharacter.hasOwnProperty(item[0])) {
        wordListByFirstCharacter[item[0]] = [item]
      } else {
        wordListByFirstCharacter[item[0]].push(item)
      }
    })

    return wordListByFirstCharacter
  }

  /**
     * Group all the words in an object using the length of the word as key
     * @param {Array} completeList - Complete list of all words
     * @return {Object} - indexed by the length of the words
     */
  getWordListByLength (completeList) {
    let wordListByLength = {}
    completeList.map(item => {
      if (!wordListByLength.hasOwnProperty(item.length)) {
        wordListByLength[item.length] = [item]
      } else {
        wordListByLength[item.length].push(item)
      }
    })

    return wordListByLength
  }

  /**
     * Check is the given word is inside the words list and return true if is an anagram
     * @param word - word to be checked if is an anagram
     * @return {boolean} - true if is an anagram
     */
  isAnagram (word) {
    if (word && typeof word === 'string' && word.length > 0 && !this.wordListByFirstCharacter.hasOwnProperty(word[0])) {
      return false
    }
    const filteredList = this.wordListByFirstCharacter[word[0]]
    for (let i = 0; i < filteredList.length; i++) {
      let wordItem = filteredList[i]
      if (word === wordItem) {
        return true
      }
    }
    return false
  }

  cleanWord (word) {
    if (word && typeof word === 'string') {
      return word.split('').sort().join('')
    }
  }
}

exports.AnagramHelper = AnagramHelper
