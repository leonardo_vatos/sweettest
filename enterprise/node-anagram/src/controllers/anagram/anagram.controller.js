// Importing libraries
const fs = require('fs')
const wordListPath = require('word-list')
const wordList = fs.readFileSync(wordListPath, 'utf8').split('\n')

// importing Classes
const AnagramHelper = require('../../helpers/AnagramHelper').AnagramHelper

// Instatiate Anagram Helper
const anagramHelper = new AnagramHelper(wordList)

exports.ping = function (req, res, next) {
  res.status(200).json('pong')
}

/**
 * @description exported function to get all anagrams words related
 * @param {string} word - word to be checked if is an Anagram
 * @return {Array<string>} - Array of words that are anagrams, empty array if is not
 */
exports.find = function (req, res, next) {
  const word = req.query.word
  if (!word || typeof word === 'undefined') {
    res.status(400).json({
      error: 'Word must be provided.'
    })
    return []
  }

  res.status(200).json(anagramHelper.getAllAnagrams(word))
}

/**
 * @description endpoint function to compare if two words are valid words and anagrams between them
 * @param {string} word1
 * @param {string} word2
 * @return {Bool}
 */
exports.compare = function (req, res, next) {
  // Gotten user from local strategy in passport configuration
  const word1 = req.query.word1
  const word2 = req.query.word2

  if ((!word1 || typeof word1 === 'undefined') || (!word2 || typeof word2 === 'undefined')) {
    res.status(400).json({
      error: 'word1 and word2 must be provided.'
    })
    return
  }

  let areAnagrams = anagramHelper.checkAnagramWords(word1, word2)
  res.status(200).json(areAnagrams)
}
