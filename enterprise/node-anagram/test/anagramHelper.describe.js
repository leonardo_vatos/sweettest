const assert = require('assert');
const AnagramHelper = require('../src/helpers/AnagramHelper').AnagramHelper;

describe('Test AnagramHelper', function() {
    let wordList = ["ab", "aaa", "bb", "ccc", "ba", "bc", "ca"];
    let anagramHelper = new AnagramHelper(wordList);

    it('test wordListByFirstCharacter', function() {
        let filteredList = {
            "a" : ["ab", "aaa", "ba"],
            "b" : ["bb", "bc"],
            "c" : ["ccc", "ca"]
        };
        assert.equal(Object.keys(anagramHelper.wordListByFirstCharacter).toString(), Object.keys(filteredList).toString());
    });

    it('test wordListByLength', function() {
        let filteredList = {
            "2" : ["ab", "bb", "ba", "bc", "ca"],
            "3" : ["aaa", "ccc"]
        };
        assert.equal(Object.keys(anagramHelper.wordListByLength).toString(), Object.keys(filteredList).toString());
    });

    it('test isAnagram', function() {
        let word = "ab";
        assert.equal(anagramHelper.isAnagram(word), true);

        let word2 = "aa";
        assert.equal(anagramHelper.isAnagram(word2), false);
    });

    it('test getAllAnagrams', function() {
        let word = "ba";
        assert.equal(anagramHelper.getAllAnagrams(word).toString(), ["ab"].toString());
    });

    it('test checkAnagramWords', function() {
        let word1 = "ba";
        let word2 = "ab";
        assert.equal(anagramHelper.checkAnagramWords(word1, word2), true);

        let word3 = "bc";
        assert.equal(anagramHelper.checkAnagramWords(word1, word3), false);
    });



});